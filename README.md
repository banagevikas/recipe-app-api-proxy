# Recipe App API Proxy

## Usage

### Enviornment Variable

* `LISTEN_PORT` - Port to listen on (default `8080`)
* `APP_HOST` - Hostname of app (default `app`)
* `APP_PORT` - Port of the app to forward to (default `9000`)